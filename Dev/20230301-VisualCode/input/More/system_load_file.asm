; ---------------------------
system_load_file:
; ---------------------------
; hl = filename ptr
; de = buffer address
; ---------------------------
	push hl
	push de

	ld b,12
	push de
	call &bc77
	pop de
	jr nc,load_error

	ex de,hl
	call &bc83
	jr nc,load_error

	call &bc7a
	jr nc,load_error

	pop de
	pop hl
	ret

load_error
	call &bc7d
	pop de
	pop hl

	jr system_load_file