; ---------------------------
system_write_file:
; ---------------------------
; hl -> filename ptr
; de -> length of data
; bc -> address of the data
; a  -> file type
; ---------------------------
	push hl
	push de
	push bc
	push af

	ld b,12
	ld de,system_write_file_buffer
	call &bc8c

	jr nc,write_error

	pop af
	pop bc
	pop de
	pop hl

	push hl
	push de
	push bc
	push af

	ld h,b
	ld l,c
	ld bc,&0000	; execution address

	call &bc98
	jr nc,write_error

	call &bc8f
	jr nc,write_error

	pop af
	pop bc
	pop de
	pop hl

	ret

write_error
	call &bc92

	pop af
	pop bc
	pop de
	pop hl

	jr system_write_file

system_write_file_buffer
	ds 200,0
