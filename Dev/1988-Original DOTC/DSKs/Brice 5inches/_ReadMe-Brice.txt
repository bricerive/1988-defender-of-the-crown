################################################################
----------------------------------------------------------------
SRC 1-2-3 are 80 tracks with even tracks used and leftovers in odd tracks

Re-transfered them with: |RDSK,"D.dsk","A",80

I'm not sure how I created them in the first place.
Looking inside with filePatch, I see the following contents:

1A & 1B
DEFAITE.ASM
VICTOIRE.ASM
INTRO.ASM
LINK.ASM
SON.ASM
SONDAT.ASM
SIEGE.ASM
ROBIN.ASM

2A:
ATTAK.ASM
DISC.ASM
BIBLI.ASM
TXT.ASM
FLECHE.ASM
INIT.ASM
MAIN.ASM
MENUS.ASM
MUSINT.ASM
POLICE.ASM
SCR.ASM
SPRITE.ASM
SQUELET.ASM