
;
;        TRACK40
;            (c)1989-BRICE
;
;Ce programme permet d'installer un loader en piste 40 sous le format DEF.
;Le loader ne doit pas depasser 1904 octets et doit etre dans un fichier .BIN .
;
         ORG  #40
         EXTRA
;
         CALL INITDISC
         SCF
MENU
         CALL NC,ERREUR
         CALL AFFICHE
         DEFB #0C,#04,#02,#1C,#00,#00
         DEFB #00,#1C,#01,#13,#13,#1D,#00,#00
         DEFB "TRACK40 : Generateur de la piste 40 de BRPROT"
         DEFB #0A,#0D,"(c) 1988 B.RIVE."
         DEFB #1F,#01,#18,"Inserez la disquette contenant le loader a installer"
         DEFB #FF
         CALL #BB18
         CP   #FC
         JP   Z,MENU
         CALL LIT
         JP   NC,MENU
         CALL AFFICHE
         DEFB #0C,#01,#18,"Inserez la disquette destination"
         DEFB #FF
         CALL #BB18
         CP   #FC
         JP   Z,MENU
         CALL ECRIT
         JP   MENU
;
ERREUR
         CALL AFFICHE
         DEFB #0C,#1F,#05,#19
         DEFB "**** ABANDON ****"
         DEFB #0A,#0A,#0A,#0A,#0A
         DEFB #0A,#0A,#0A,#0A,#0A
         DEFB #0A,#0A,#0A,#0A,#0A
         DEFB #0A,#0A,#0A,#0A,#0A
         DEFB #0A,#0A,#0A,#0A,#0A
         DEFB #FF
         RET
;
;
ECRIT
         LD   HL,BUFFER
         LD   (HL),0
         LD   DE,BUFFER+1
         LD   BC,#1000-1
         LDIR
         LD   HL,DEFFORM
         LD   D,40
         OR   A
         CALL SEEK
         RET  NC
         CALL FORMAT
         RET  NC
         PUSH HL
         LD   HL,BUFFER
         CALL DEFWRITE
         POP  HL
         RET
;
;
;
LIT
         LD   A,3
         LD   (STYPE),A
         CALL AFFICHE
         DEFB #1F,1,24,#12,#FF
         CALL SELFICH
         RET  NC
         CALL READFICH
         RET
;
;*******************************************************************************
;ROUTINES DE NIVEAU 2
;*******************************************************************************
;
SELFICH
         XOR  A
         LD   (NBFICH),A
         LD   A,(STYPE)
         CALL READDIR
         RET  NC
         CALL AFFICHE
         DEFB #1F,1,5,#FF
         CALL AFFDIR
         RET  NC
         CALL SELECT
         RET
;
READDIR
         LD   D,0
         LD   E,#C1
AMSRDDI1 LD   B,4
         LD   HL,DIRBUF
         CALL SEEK
         RET  NC
AMSRDDI0 CALL READ
         RET  NC
         INC  E
         DJNZ AMSRDDI0
         SCF
         RET
;
AFFDIR
         LD   IY,DIRBUF
         LD   B,64
         LD   DE,32
AMSAD0   LD   A,(IY+00)
         CP   16
         JR   NC,AMSAD1
         LD   A,(IY+12)
         OR   A
         JR   NZ,AMSAD1
         PUSH IY
         POP  HL
         INC  HL
         CALL AFFNAME
         LD   HL,NBFILE
         INC  (HL)
AMSAD1   ADD  IY,DE
         DJNZ AMSAD0
         LD   A,(NBFILE)
         OR   A
         RET  Z
         SCF
         RET
;
AFFNAME
         PUSH HL
         PUSH BC
         LD   A,9
         CALL #BB5A
         CALL #BB5A
         LD   B,8
         CALL AFFNAM0
         LD   A,"."
         CALL #BB5A
         LD   B,3
         CALL AFFNAM0
         LD   A,9
         CALL #BB5A
         CALL #BB5A
         POP  BC
         POP  HL
         RET
AFFNAM0  LD   A,(HL)
         INC  HL
         AND  #7F
         CALL #BB5A
         DJNZ AFFNAM0
         RET
;
;
READFICH
         LD   E,64
         LD   IX,DIRBUF
         LD   BC,32
AMSRDF0  LD   A,(IX+00)
         CP   16
         JR   NC,AMSRDF1
         LD   A,(IX+12)
         OR   A
         JR   NZ,AMSRDF1
         LD   A,E
         OR   A
         JR   Z,AMSRDF2
         DEC  E
AMSRDF1  ADD  IX,BC
         JR   AMSRDF0
AMSRDF2  PUSH IX
         POP  HL
         INC  HL
         CALL AFFICHE
         DEFB #1F,1,23
         DEFB " Lecture en cours : ",8,8,#FF
         CALL PUTNAME
         CALL AFFNAME
         LD   HL,FILENAME
         LD   B,12
         CALL #BC77
         RET  NC
         LD   (LONGUEUR),BC
         LD   HL,6000-#1000
         OR   A
         SBC  HL,BC
         JR   NC,READFOK
         CALL AFFICHE
         DEFB #1F,#01,#18
         DEFB "Fichier loader trop long..."
         DEFB #FF
         CALL #BB18
         OR   A
         RET
READFOK  LD   HL,FICHIER
         CALL #BC83
         RET  NC
         CALL #BC7A
         RET
;
PUTNAME
         PUSH HL
         PUSH BC
         PUSH DE
         LD   DE,FILENAME
         LD   BC,8
         LDIR
         LD   A,"."
         LD   (DE),A
         INC  DE
         LD   BC,3
         LDIR
         POP  DE
         POP  BC
         POP  HL
         RET
;
;
;
;*******************************************************************************
;ROUTINES DE DIALOGUE AVEC LE FDC 765
;*******************************************************************************
;
FORMAT
         DI
         PUSH BC
         PUSH HL
         PUSH DE
         LD   B,19
         LD   A,D
         LD   DE,4
         LD   HL,DEFFORM
FORMBCL  LD   (HL),A
         ADD  HL,DE
         DJNZ FORMBCL
         POP  DE
         POP  HL
         PUSH HL
         LD   A,#4D
         CALL FDCOUT
DRIVE1   LD   A,#00
         CALL FDCOUT
         LD   A,#02
         CALL FDCOUT
         LD   A,#09
         CALL FDCOUT
         LD   A,#50
         CALL FDCOUT
         LD   A,#AA
         CALL FDCOUT
         CALL DATAOUT
         CALL RESULT
         LD   HL,FORMRES
         CALL TSTRESU
         POP  HL
         POP  BC
         RET

DEFFORM  DEFB #00,#00,#00,#06
;
READ
         PUSH BC
         DI   
         LD   A,#46                     ;Lire donnees
         CALL FDCOUT
DRIVE3   LD   A,0
         CALL FDCOUT
         LD   A,D                       ;Piste
         CALL FDCOUT
         LD   A,#00                     ;Lecteur
         CALL FDCOUT
         LD   A,E                       ;Secteur
         CALL FDCOUT
         LD   A,2                       ;Taille
         CALL FDCOUT
         LD   A,E
         CALL FDCOUT
         LD   A,#2A
         CALL FDCOUT
         LD   A,#FF
         CALL FDCOUT
         CALL DATAIN
         CALL RESULT
         PUSH HL
         LD   HL,AMSRRES
         CALL TSTRESU
         POP  HL
         POP  BC
         RET  
;
DEFWRITE
         PUSH BC
         PUSH DE
         DI   
         LD   A,#45                     ;Ecrire donnees
         CALL FDCOUT
DRIVE5   LD   A,0
         CALL FDCOUT
         LD   A,D
         CALL FDCOUT
         LD   A,0
         CALL FDCOUT
         LD   A,0
         CALL FDCOUT
         LD   A,6
         CALL FDCOUT
         LD   A,0
         CALL FDCOUT
         LD   A,#2A
         CALL FDCOUT
         LD   A,#FF
         CALL FDCOUT
         LD   DE,PISTSIZE+4
         CALL NDATAOUT
         CALL RESULT
         PUSH HL
         LD   HL,DEFWRES
         CALL TSTRESU
         LD   BC,#F8F8
         LD   L,#FF
         OUT  (C),L
         LD   BC,#EF00
         OUT  (C),C
         POP  HL
         POP  DE
         POP  BC
         RET  
;
;
SEEK
         PUSH BC
         PUSH DE
         PUSH HL
DRIVE7   LD   E,0
         RST  #18
         DEFW SEEKCOM
         POP  HL
         POP  DE
         POP  BC
         RET

SEEKCOM  DEFW #C763
         DEFB #07
;
FDCOUT
         LD   BC,#FB7E
         PUSH AF
SAITRQM  IN   A,(C)
         ADD  A,A
         JR   NC,WAITRQM
         ADD  A,A
         JR   NC,OKOUT
         POP  AF
         RET  
OKOUT    POP  AF
         INC  C
         OUT  (C),A
         DEC  C
         LD   A,05
FDCTEMP  DEC  A
         NOP
         JR   NZ,FDCTEMP
         RET  
;
RESULT
         PUSH HL
         LD   HL,RESBUF
RESAGAIN IN   A,(C)
         CP   #C0
         JR   C,RESAGAIN
         INC  C
         IN   A,(C)
         DEC  C
         LD   (HL),A
         INC  HL
         LD   A,#05
RESTEMP  DEC  A
         JR   NZ,RESTEMP
         IN   A,(C)
         AND  #10
         JR   NZ,RESAGAIN
         POP  HL
         RET
;
AFFRESU
         PUSH HL
         PUSH BC
         PUSH AF
         CALL AFFICHE
         DEFB #1F,60,1,#FF
         LD   B,3
         LD   HL,RESBUF
AFFRESU0 LD   A,(HL)
         CALL AFFA
         LD   A," "
         CALL #BB5A
         INC  HL
         DJNZ AFFRESU0
         CALL #BB18
         POP  AF
         POP  BC
         POP  HL
         RET
;
TSTRESU
         PUSH DE
         PUSH BC
         LD   DE,RESBUF
         LD   B,3
TSTRESU0 LD   A,(DE)
         CP   (HL)
         SCF
         JR   NZ,TSTRESU1
         INC  HL
         INC  DE
         DJNZ TSTRESU0
         OR   A
TSTRESU1 CCF
         POP  BC
         POP  DE
         RET

FORMRES  DEFB #00,#00,#00
DEFWRES  DEFB #40,#80,#00
RDIDRES  DEFB #00,#00,#00
DEFRRES  DEFB #40,#20,#20
AMSRRES  DEFB #40,#80,#00
AMSWRES  DEFB #40,#80,#00
;
DATAIN
         LD   BC,#FB7E
         JR   DATAIN1
DATAIN0  INC  C
         IN   A,(C)
         LD   (HL),A
         DEC  C
         INC  HL
DATAIN1  IN   A,(C)
         JP   P,DATAIN1
         AND  #20
         JR   NZ,DATAIN0
         RET  
;
DATAOUT
         LD   BC,#FB7E
         JR   DATAOU1
DATAOU0  INC  C
         LD   A,(HL)
         OUT  (C),A
         DEC  C
         INC  HL
DATAOU1  IN   A,(C)
         JP   P,DATAOU1
         AND  #20
         JR   NZ,DATAOU0
         RET  
;
NDATAOUT
         LD   BC,#FB7E
         JR   NDATAOU1
NDATAOU0 INC  C
         LD   A,(HL)
         OUT  (C),A
         INC  HL
         DEC  DE
         LD   A,D
         OR   E
         JR   Z,NDATAOU2
         DEC  C
NDATAOU1 IN   A,(C)
         JP   P,NDATAOU1
         AND  #20
         JR   NZ,NDATAOU0
         RET
NDATAOU2 LD   BC,#F8F8
         XOR  A
         OUT  (C),A
         LD   BC,#EFFF
         OUT  (C),C
         LD   BC,#FB7F
         OUT  (C),A
         DEC  C
FDCCLR0  IN   A,(C)
         JP   P,FDCCLR0
         BIT  5,A
         RET  Z
         INC  C
         IN   A,(C)
         OUT  (C),A
         DEC  C
         JR   FDCCLR0
;
INITDISC
         LD   C,7
         LD   DE,#40
         LD   HL,#B0FF
         CALL #BCCE
         LD   HL,COMMAND
         CALL #BCD4
         XOR  A
         JP   #1B

COMMAND  DEFB "DIS","C"+#80
;
;
;*******************************************************************************
;ROUTINES DE DIALOGUE AVEC L'UTILISATEUR
;*******************************************************************************
;
SELECT
         LD   HL,SELTAB
         LD   B,64
SELECTI  LD   (HL),0
         INC  HL
         DJNZ SELECTI
         LD   E,1
         CALL INVERT
SELECT0  CALL #BB18
         CP   #FC
         RET  Z
         CP   #F2
         JR   NZ,SELECT1
         LD   A,E
         CP   1
         JR   Z,SELECT0
         CALL INVERT
         DEC  E
         CALL INVERT
         JR   SELECT0
SELECT1  CP   #F3
         JR   NZ,SELECT2
SELECT7  LD   A,(NBFILE)
         CP   E
         JR   Z,SELECT0
         CALL INVERT
         INC  E
         CALL INVERT
         JR   SELECT0
SELECT2  CP   #F1
         JR   NZ,SELECT3
         LD   A,(NBFILE)
         LD   D,A
         LD   A,E
         ADD  A,5
         INC  D
         CP   D
         JR   NC,SELECT0
         CALL INVERT
         LD   E,A
         CALL INVERT
         JR   SELECT0
SELECT3  CP   #F0
         JR   NZ,SELECT4
         LD   A,E
         SUB  6
         JR   C,SELECT0
         CALL INVERT
         INC  A
         LD   E,A
         CALL INVERT
         JR   SELECT0
SELECT4  CP   #20
         JR   NZ,SELECT5
         CALL MARK
         LD   HL,SELTAB-1
         LD   D,0
         ADD  HL,DE
         LD   A,(HL)
         XOR  #FF
         LD   (HL),A
         LD   HL,NBFICH
         JR   Z,SELECT6
         INC  (HL)
         JR   SELECT7
SELECT6  DEC  (HL)
         JR   SELECT7
SELECT5  CP   #0D
         JR   NZ,SELECT0
         LD   A,(NBFICH)
         OR   A
         RET  Z
         SCF
         RET

SELTAB   DEFS 64
;
INVERT
         PUSH AF
         LD   HL,#0004
         LD   A,E
         DEC  A
INVERT0  SUB  5
         JR   C,INVERT1
         INC  L
         JR   INVERT0
INVERT1  ADD  A,5
         SLA  A
         SLA  A
         SLA  A
         SLA  A
         ADD  A,H
         LD   H,A
         CALL #BC1A          ;SCR CHAR POSITION
         LD   C,8
INVERT2  LD   B,16
         PUSH HL
INVERT3  LD   A,(HL)
         XOR  #FF
         LD   (HL),A
         INC  HL
         DJNZ INVERT3
         POP  HL
         CALL #BC26          ;SCR NEXT LINE
         DEC  C
         JR   NZ,INVERT2
         POP  AF
         RET
;
MARK
         PUSH AF
         PUSH DE
         LD   HL,#0004
         LD   A,E
         DEC  A
MARK0    SUB  5
         JR   C,MARK1
         INC  L
         JR   MARK0
MARK1    ADD  A,5
         SLA  A
         SLA  A
         SLA  A
         SLA  A
         ADD  A,H
         LD   H,A
         CALL #BC1A          ;SCR CHAR POSITION
         LD   C,8
         LD   DE,13
MARK2    PUSH HL
         CALL MARK3
         ADD  HL,DE
         CALL MARK3
         POP  HL
         CALL #BC26          ;SCR NEXT LINE
         DEC  C
         JR   NZ,MARK2
         POP  DE
         POP  AF
         RET
MARK3    LD   A,(HL)
         XOR  #55
         LD   (HL),A
         INC  HL
         LD   A,(HL)
         XOR  #55
         LD   (HL),A
         RET
;
EDIT
         LD   (POINTE+1),HL
         PUSH BC
         XOR  A
         LD   (INDICE),A
         LD   A,32
NEWTXT   LD   (HL),A
         INC  HL
         DJNZ NEWTXT
         POP  BC
EDITBCLE PUSH BC
         LD   HL,(POINTE+1)
AFFTXT   LD   A,(HL)
         CALL #BB5A
         INC  HL
         DJNZ AFFTXT
         POP  BC
         PUSH BC
AFFBCK   LD   A,#08
         CALL #BB5A
         DJNZ AFFBCK
         POP  BC
         CALL #BB18
         CP   #0D                           ;RETURN
         JR   Z,EDEND
         CP   #8B                           ;ENTER
         JR   Z,EDEND
         CP   #FC                           ;ESCAPE
         RET  Z
         CP   #7F                           ;DELETE
         JR   NZ,NOTDEL
         LD   A,(INDICE)
         OR   A
         JR   Z,EDITBCLE
         CALL POINTE
         LD   (HL),32
         LD   HL,INDICE
         DEC  (HL)
         JR   EDITBCLE
NOTDEL   CP   #20
         JR   C,EDITBCLE
         LD   HL,INDICE
         LD   C,A
         LD   A,(HL)
         CP   B
         JR   Z,EDITBCLE
         INC  (HL)
         CALL POINTE
         LD   (HL),C
         JR   EDITBCLE
EDEND    LD   A,(INDICE)
         LD   B,A
         SCF
         RET
POINTE
         LD   HL,0000
         LD   A,(INDICE)
         DEC  A
         ADD  A,L
         LD   L,A
         RET  NC
         INC  H
         RET
;
AFFHL
         LD   A,H
         CALL AFFA
         LD   A,L
AFFA
         PUSH AF
         RRA
         RRA
         RRA
         RRA
         CALL AFFDIGIT
         POP  AF
AFFDIGIT
         AND  #0F
         ADD  A,"0"
         CP   ":"
         JR   C,OKNUMBER
         ADD  A,7
OKNUMBER JP   #BB5A
;
AFFICHE
         EX   (SP),HL
AFFICH1  LD   A,(HL)
         INC  HL
         CP   #FF
         JR   NZ,AFFICH0
         EX   (SP),HL
         RET  
AFFICH0  CALL #BB5A
         JR   AFFICH1
;
PISTSIZE EQU  6000
;
NBFILE   DEFB 0
NBFICH   DEFB 0
;
DSKDIR   DEFS 2
ADRESSE  DEFS 3
LONGUEUR DEFS 2
         DEFB "A:"
FILENAME DEFS 12
INDICE   DEFS 1
DIRPNT   DEFS 2
DOURCE   DEFB 0
DESTIN   DEFB 0
SAME     DEFB 0
STYPE    DEFS 1
DTYPE    DEFS 1
;
RESBUF   DEFS 20
DIRBUF   DEFS 2048
BUFFER   DEFS #1000
FICHIER  EQU  $
;













