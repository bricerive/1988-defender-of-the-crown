
;
;Comparaison de deux fichiers ASCII
;
         ORG  #40
;
         LD   A,2
         CALL #BC0E
         LD   BC,#0000
         CALL #BC38
         LD   A,1
         LD   BC,#1313
         CALL #BC32
         LD   A,0
         LD   BC,#0000
         CALL #BC32
         LD   C,7
         LD   HL,#B100
         LD   DE,#40
         CALL #BCCE
         LD   DE,#0800
         OR   A
         SBC  HL,DE
         LD   (DATA1+1),HL
         LD   (DATA2+1),HL
CHOIX
         CALL MESS
         DEFB #0C,"Nom du fichier ASCII a comparer: ",#FF
         LD   B,12
         LD   HL,NAME
         CALL EDIT
         LD   A,B
         LD   (LONG1+1),A
         LD   (LONG2+1),A
LECTUR1
         CALL AFFNAME
         CALL MESS
         DEFB #0D,#0A,"Inserez le disque 1"
         DEFB #FF
         CALL #BB06
         LD   HL,NAME
LONG1    LD   B,12
DATA1    LD   DE,#0000
         CALL #BC77
         JP   NC,CHOIX
         LD   HL,BUFFER
READ     CALL #BC80
         JR   NC,TOUTLU
         LD   (HL),A
         INC  HL
         JR   READ
TOUTLU
         CALL #BC7A
LECTUR2
         CALL MESS
         DEFB #0D,#0A,"Inserez le disque 2"
         DEFB #FF
         CALL #BB06
         LD   HL,NAME
LONG2    LD   B,12
DATA2    LD   DE,#0000
         CALL #BC77
         JP   NC,CHOIX
         LD   HL,BUFFER
BOUCLE
         CALL #BC80
         JR   NC,END
         CP   (HL)
         JR   NZ,DIFF
         INC  HL
         JR   BOUCLE
DIFF
         CALL MESS
         DEFB #0D,#0A,"--->ERROR: ",#FF
         PUSH HL
         CALL #BB5D
         POP  HL
         LD   A,255
         CALL #BB5A
         LD   A,(HL)
         PUSH HL
         CALL #BB5D
         POP  HL
         INC  HL
         CALL #BB1B
         JR   NC,NOKEY
         CP   "S"
         JR   Z,END
         CP   "s"
         JR   Z,END
NOKEY
         JP   BOUCLE

END
         CALL #BC7A
         CALL MESS
         DEFB #0D,#0A,"FIN.",#FF
         CALL #BB06
         JP   CHOIX
         RET
;
AFFNAME
         CALL MESS
         DEFB #0C,"Nom du fichier :"
NAME     DEFB "ATTAK   .ASM"
         DEFB #FF
         RET
;
MESS
         EX   (SP),HL
         PUSH AF
MESS1    LD   A,(HL)
         INC  HL
         CP   #FF
         CALL NZ,#BB5A
         JR   NZ,MESS1
         POP  AF
         EX   (SP),HL
         RET
;
;EDIT(B:Nb char,HL:Addr)
;
EDIT
         LD   (POINTE+1),HL
         PUSH BC
         XOR  A
         LD   (INDICE),A
         LD   A,32
NEWTXT   LD   (HL),A
         INC  HL
         DJNZ NEWTXT
         POP  BC
EDITBCLE PUSH BC
         LD   HL,(POINTE+1)
AFFTXT   LD   A,(HL)
         CALL #BB5A
         INC  HL
         DJNZ AFFTXT
         POP  BC
         PUSH BC
AFFBCK   LD   A,#08
         CALL #BB5A
         DJNZ AFFBCK
         POP  BC
         CALL #BB18
         CP   #0D                           ;RETURN
         JR   Z,EDEND
         CP   #8B                           ;ENTER
         JR   Z,EDEND
         CP   #FC                           ;ESCAPE
         RET  Z
         CP   #7F                           ;DELETE
         JR   NZ,NOTDEL
         LD   A,(INDICE)
         OR   A
         JR   Z,EDITBCLE
         CALL POINTE
         LD   (HL),32
         LD   HL,INDICE
         DEC  (HL)
         JR   EDITBCLE
NOTDEL   CP   #20
         JR   C,EDITBCLE
         LD   HL,INDICE
         LD   C,A
         LD   A,(HL)
         CP   B
         JR   Z,EDITBCLE
         INC  (HL)
         CALL POINTE
         LD   (HL),C
         JR   EDITBCLE
EDEND    LD   A,(INDICE)
         LD   B,A
         SCF
         RET
POINTE
         LD   HL,0000
         LD   A,(INDICE)
         DEC  A
         ADD  A,L
         LD   L,A
         RET  NC
         INC  H
         RET
;
INDICE   DEFS 1
;
BUFFER   EQU  $



